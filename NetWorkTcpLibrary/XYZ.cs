﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetWorkTcpLibrary
{
    [ProtoContract]
    public class XYZ
    {       
        [ProtoMember(1)]
        public float _X;
        [ProtoMember(2)]
        public float _Y;
        [ProtoMember(3)]
        public float _Z;
        [ProtoMember(4)]
        public List<XYZ> xYZs = new List<XYZ>();

        public void xYZsAdd(XYZ xYZ)
        {
            xYZs.Add(xYZ);
        }

    }
}
